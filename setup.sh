#!bash

## Download the data from GDrive

export fileid=1x5-RUrUbEGwP9iVdkHkZ72hTiiwq6rF9
export filename=siim-train-test.zip

## WGET ##
wget --save-cookies cookies.txt 'https://docs.google.com/uc?export=download&id='$fileid -O- \
     | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1/p' > confirm.txt

wget --load-cookies cookies.txt -O $filename \
     'https://docs.google.com/uc?export=download&id='$fileid'&confirm='$(<confirm.txt)

## Unzip them
unzip -q -n siim-train-test.zip

## Install requirements
pip3 install -r requirements.txt
