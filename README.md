# [SIIM-ACR Pneumothorax Segmentation](https://www.kaggle.com/c/siim-acr-pneumothorax-segmentation), a Kaggle (Weekend) challenge

![sample-prediction](images/predicted_se_resnext_50.png)

This repository contains my take on the SIIM-ACR Pneumothorax Classification and Segmentation Kaggle challenge that run from June to September 2019.

I did not take part in the original challenge, but I decided to test my knowledge against it because it's fairly recent and because I wanted to tackle a medical imaging problem.

Since the competition is already over, I also decided I'd raise the challenge a little bit, by constraining myself to find a reasonable solution in just a weekend. More in-depth motivations are [at the bottom](#motivation) of the README.

The problem
----

We are given [a set of chest X-Rays](https://www.kaggle.com/c/siim-acr-pneumothorax-segmentation/data) and are tasked to identify potential cases of [Pneumothorax](https://en.wikipedia.org/wiki/Pneumothorax) (also called Collapsed Lungs).

The resulting prediction should be both a classification label (presence or absence of PT) and, in case of a positive diagnosis, a segmentation mask aimed at highlighting the interested region in the original image.

Such a tool would be of course helpful in giving a fast, data-driven, second opinion to a trained radiologists, enhancing his/her diagnosis.

My Solution
----

Semantic Segmentation to me screams [UNet](https://arxiv.org/pdf/1505.04597.pdf). While the paper is rather old (in Deep Learning years) the main idea is aging very well by just improving the encoder part of the architecture as better Computer Vision models are released.

[DeepLabV3+](https://arxiv.org/pdf/1802.02611.pdf) is a more recent competitor that has been doing really well, but the available implementation in PyTorch's [torchvision](https://pytorch.org/docs/stable/torchvision/models.html#deeplabv3) is only DeepLabV3 (no + !) and the backbones start at ResNet50, making it hard to quickly prototype and run on Colab or locally.

[MaskRCNN](https://arxiv.org/abs/1703.06870) could also be employed, fusing the segmented instances into just one map; the added layer of complexity of either messing with data annotations and / or model outputs felt though less appealing for this particular task.

I thus finally settled on a multi-task UNet architecture, with [SE ResNeXt 50](https://arxiv.org/abs/1709.01507) backbone (a ResNet34 variant worked initially surprisingly well despite being quite shallow) and an extra classification head, stemming from the end of the encoder (see [here](https://gitlab.com/mbant/siim-acr-pt-segmentation/blob/master/SIIM_class.ipynb), on cell 9, for example).

![UNet-image](images/unet.png)

The network is trained with a weighted mixed loss, made up of a Dice loss for the segmentation task for the most part plus Balanced Cross Entropy losses both on the pixel level and on classification task. (Adam optimizer, batch size of 12 for the shallower network or 8 for the deeper ones).

I used a [One Cycle](https://arxiv.org/abs/1708.07120) learning rate scheduler, with Cosine annealing. Also, I increase the proportion of negative samples (as in without Pneumothorax) linearly from 0 to ~78% (congruently with the initial dataset).

I tried several data augmentations but in the end settled on simple pipeline of Adaptive histogram equalization (CLAHE), Horizontal and Vertical flips, Resize (to from squares with side `1024` to a side of `1024//4*3 = 768`) and Random Crop (`512x512`).
Plus the obvious Normalization, to transform the images to a space more similar to the pretraining one (imagenet), and Torch Tensor transformation.

At inference time I use just a combination of Resizing and Normalization plus (TTA) Horizontal and Vertical Flips, and average the results.

All my solutions were prototyped on [Google Colaboratory](https://colab.research.google.com/) and later trained and refined on [Google Cloud Platform](https://cloud.google.com) with 2 NVIDIA Tesla P100 GPUs.

See the [`setup.sh`](./setup.sh) script to download the data and install all relevant python packages (from [`requirements.txt`](./requirements.txt)) for the remote instance. A Docker image would have worked as well, but I liked playing around in notebooks.

Shortcomings
----

Due to the self-imposed time constraint of 'one weekend', my solution has a few clear shortcomings. A couple of easy but dramatically time-consuming improvements for example are K-Fold Cross Validation and Ensambling.

Only ~ 22% of the images in the training set actually are positive to PT, and so Cross Validation would surely help by letting the model train on all the available ones (usually I'd train a model on each fold and later ensamble the top ones for inference), rather than keeping 15% of that for validation (I used stratified sampling to maintain similar proportion of positive cases n both the train and validation sets).
Training each model K times (once for each fold) though would have increased the training time well over the time I had in mind.

Ensambling, *i.e.* training multiple models on the task and aggregating their solutions during inference, even without Cross Validation, was another potential source of improvements. I did try several and solutions before settling on a particular one, for example I tried to [train the backbone for the classification task first](./SIIM_class.ipynb), with the idea of tackling the segmentation problem with an encoder already fine-tuned on this task's images, and several architectures as well, but the se_resnext50-unet worked so much better than the other networks I tried (variants of [EfficientNet](https://ai.googleblog.com/2019/05/efficientnet-improving-accuracy-and.html)
 for example) that ensambling makes little sense at this point.

Results
----

Visually, the model *feels* right, as it clearly marks areas around the lungs in each image ... this is all I can say being completely untrained in radiology.

![visual-output](images/multiple_preds.png)

We can though submit our results in the prescribed submission format (a csv file) and see what the Kaggle grader says!

![score](images/score.png)

a very positive result I'd say, as this puts me right around the 85<sup>th</sup> place (with ~1500 teams participating)! With only ~8 hours of effective training on GCP and less then 2 days overall to work on the solution (*) we're well within the fist 10%! 

![leaderboard](images/leaderboard.png)


*(\*) and the benefit of hindsight of course, the competition being already over.*

<a name="motivation"></a>
## Motivation

Although I had experience in Computer Vision on both images and videos(image classification and semantic/instance segmentation), I never tackled first-hand a joint semantic segmentation and classification problem. Moreover, I wanted to gain some exposure to medical imaging, so I decided to challenge myself on a full-immersive weekend trying to come up with a working solution for [this](https://www.kaggle.com/c/siim-acr-pneumothorax-segmentation) kaggle challenge.

The challenge is now over and I couldn't really hope to compete with the posted solutions (developed over the months during which the challenge lasted ) in just two days, but I wanted to give it a shot regardless!

I am particularly please on how I got to learn a lot about the [DICOM standard](https://en.wikipedia.org/wiki/DICOM) before starting this challenge; I had researched about it in the past, but fist-hand exploration (it in the [first notebook](./SIIM_explore.ipynb)) always leaves me with more insights and sedimented knowledge. 


### Acknowledgements

Coding everything from scratch is usually a bad idea (unless you're otherwise not understanding what's going on!), not only because Deep Learning in particular thrives on open-source software (and plenty of smart people have surely wrote exactly the library you need) but in particular this time because of the time constraint I wouldn't have been able to start from a clean slate.

So thanks to:

* [Segmentation models PyTorch](https://github.com/qubvel/segmentation_models.pytorch) for accessing the pretrained models! I adapted some of the training functions to my needs and used his [TTA](https://github.com/qubvel/ttach) library as well!

* [Miras Amir (@amirassov)](https://www.kaggle.com/c/siim-acr-pneumothorax-segmentation/discussion/108397) that took 4th place in the original competition with a simple-yet-elegant solution, without requiring too much hand-tweaking or ad-hoc solutions; his process inspired heavily what I ended up using here.

* [Schlerp](http://blog.schlerp.net/) for his initial [Data Exploration Kernel](https://www.kaggle.com/schlerp/getting-to-know-dicom-and-the-data/data) on another Kaggle competition involving DICOM data and ( and [Jesper Dramsh](https://dramsch.net/) for his later take on the same topic )



